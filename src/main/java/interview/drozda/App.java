package interview.drozda;

import interview.drozda.csv.input.JiraBugsReader;
import interview.drozda.csv.output.FileWriter;
import interview.drozda.report.BugReport;


public class App {


    // task with id  MCPU-13716 is assigned to two teams - TEAM_LOIRE, TEAM_REGSERV

    private static final String INPUT_FILE_PATH = "/interview/drozda/bugs-1907.csv";
    private static final String DEFAULT_OUTPUT_FILE_NAME = "bug_summary_" + System.currentTimeMillis() + ".csv";

    public static void main(String[] args) {
        BugReport bugReport = new BugReport();

        new JiraBugsReader(INPUT_FILE_PATH, bugReport::process)
                .consumeInputFile();

        String outputFileName = args.length > 0 ? args[0] : DEFAULT_OUTPUT_FILE_NAME;

        FileWriter.writeAllLines(outputFileName, bugReport.lines());
    }

}
