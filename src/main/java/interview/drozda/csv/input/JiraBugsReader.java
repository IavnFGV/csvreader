package interview.drozda.csv.input;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.*;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class JiraBugsReader {
    private final String filePath;
    private final Consumer<JiraInputRecord> jiraInputRecordConsumer;

    public JiraBugsReader(String filePath, Consumer<JiraInputRecord> jiraInputRecordConsumer) {
        this.filePath = filePath;
        this.jiraInputRecordConsumer = jiraInputRecordConsumer;
    }

    public void consumeInputFile() {
        try (InputStream inputStream = this.getClass().getResourceAsStream(filePath);
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
             Reader reader = new BufferedReader(inputStreamReader)
        ) {
            Stream<JiraInputRecord> jiraImportRecordStream = getJiraInputRecordStream(reader);
            jiraImportRecordStream
                    .forEach(jiraInputRecordConsumer);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private Stream<JiraInputRecord> getJiraInputRecordStream(Reader reader) {
        CsvToBean<JiraInputRecord> csvToBean = new CsvToBeanBuilder<JiraInputRecord>(reader)
                .withType(JiraInputRecord.class)
                .build();

        return StreamSupport.stream(csvToBean.spliterator(), false);
    }

}
