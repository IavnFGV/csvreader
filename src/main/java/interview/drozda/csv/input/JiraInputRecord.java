package interview.drozda.csv.input;

import com.opencsv.bean.CsvCustomBindByName;
import interview.drozda.csv.input.converter.JiraIsClosedStatusConverter;
import interview.drozda.csv.input.converter.JiraPriorityConverter;
import interview.drozda.csv.input.converter.JiraTeamNameConverter;
import interview.drozda.domain.JiraPriority;
import interview.drozda.domain.JiraTeam;

public class JiraInputRecord {

    @CsvCustomBindByName(column = "Priority", converter = JiraPriorityConverter.class)
    private JiraPriority priority;
    @CsvCustomBindByName(column = "Labels", converter = JiraTeamNameConverter.class)
    private JiraTeam team;
    @CsvCustomBindByName(column = "Status", converter = JiraIsClosedStatusConverter.class)
    private boolean isClosed;

    public JiraPriority getPriority() {
        return priority;
    }

    public void setPriority(JiraPriority priority) {
        this.priority = priority;
    }

    public JiraTeam getTeam() {
        return team;
    }

    public void setTeam(JiraTeam team) {
        this.team = team;
    }

    @Override
    public String toString() {
        return "JiraInputRecord{" +
                ", priority=" + priority +
                ", team=" + team +
                ", isClosed=" + isClosed +
                '}';
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }
}
