package interview.drozda.csv.input.converter;

import com.opencsv.bean.AbstractBeanField;
import interview.drozda.csv.input.JiraInputRecord;
import interview.drozda.domain.Constants;

public class JiraIsClosedStatusConverter extends AbstractBeanField<JiraInputRecord> {
    @Override
    protected Object convert(String value) {
        return Constants.CLOSED_COMPLETE_STATUS_NAME.equals(value);
    }
}
