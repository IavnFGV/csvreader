package interview.drozda.csv.input.converter;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import interview.drozda.csv.input.JiraInputRecord;
import interview.drozda.domain.JiraPriority;

public class JiraPriorityConverter extends AbstractBeanField<JiraInputRecord> {

    @Override
    protected Object convert(String value) throws CsvDataTypeMismatchException {
        return JiraPriority.of(value)
                .orElseThrow(() -> new CsvDataTypeMismatchException("There is no String representation of JiraPriority enum in " + value));
    }
}
