package interview.drozda.csv.input.converter;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.bean.BeanFieldSingleValue;
import com.opencsv.bean.OpencsvUtils;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import interview.drozda.csv.input.JiraInputRecord;
import interview.drozda.domain.Constants;
import interview.drozda.domain.JiraTeam;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JiraTeamNameConverter extends AbstractBeanField<JiraInputRecord> {

    private static boolean last_preffered = true; // need to manage case when few treams are assigned to task

    private static String JIRA_TEAM_NAME_CAPTURE =
            (last_preffered ? Constants.ANY_SYMBOL_AND_NEW_WORD_REGEXP_GREEDY :
                    Constants.ANY_SYMBOL_AND_NEW_WORD_REGEXP_LAZY) +
                    Constants.OPEN_BRACKET +
                    String.join(Constants.OR_DELIMITER_REGEXP, JiraTeam.allTeamNames())
                    + Constants.CLOSE_BRACKET +
                    Constants.NEW_WORD_AND_END_OF_STRING_REGEXP;
    private final Pattern pattern;


    public JiraTeamNameConverter() {
        pattern = OpencsvUtils.compilePatternAtLeastOneGroup(
                JIRA_TEAM_NAME_CAPTURE, 0, BeanFieldSingleValue.class, this.errorLocale);
    }

    @Override
    protected Object convert(String value) throws CsvDataTypeMismatchException {
        Matcher m = pattern.matcher(value);
        if (m.matches()) {
            return JiraTeam.of(m.group(1))
                    // we should never get here
                    .orElseThrow(() -> new CsvDataTypeMismatchException("There is no String representation of JiraTeam enum in " + m.group(1)));
        } else {
            throw new CsvDataTypeMismatchException("There should be at least one name of team in field");
        }
    }
}
