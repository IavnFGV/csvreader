package interview.drozda.csv.output;

import com.opencsv.CSVWriter;

import java.io.IOException;
import java.util.List;

public final class FileWriter {
    private FileWriter() {
        throw new AssertionError("There is no instances of utility class");
    }

    public static void writeAllLines(String filePath, List<String[]> linesToWrite) {
        try (CSVWriter writer = new CSVWriter(new java.io.FileWriter(filePath))) {
            writer.writeAll(linesToWrite, false);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
