package interview.drozda.domain;

public class Constants {


    public static final String EMPTY_STRING = "";
    public static final String ANY_SYMBOL_AND_NEW_WORD_REGEXP_LAZY = "^.*?\\b";
    public static final String ANY_SYMBOL_AND_NEW_WORD_REGEXP_GREEDY = "^.*\\b";
    public static final String CLOSE_BRACKET = ")";
    public static final String OPEN_BRACKET = "(";
    public static final String NEW_WORD_AND_END_OF_STRING_REGEXP = "\\b.*$";
    public static final String OR_DELIMITER_REGEXP = "|";
    public static final String CLOSED_COMPLETE_STATUS_NAME = "Closed - Complete";
}
