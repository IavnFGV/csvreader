package interview.drozda.domain;

import java.util.Optional;
import java.util.stream.Stream;

public enum JiraPriority {
    BLOCKER("Blocker"),
    CRITICAL("Critical"),
    MAJOR("Major"),
    MEDIUM("Medium"),
    MINOR("Minor"),
    NORMAL("Normal");

    private String priorityName;

    JiraPriority(String priorityName) {
        this.priorityName = priorityName;
    }

    public static Optional<JiraPriority> of(String toParse) {
        return Stream.of(JiraPriority.values())
                .filter(jiraPriority -> jiraPriority.priorityName.equals(toParse))
                .findAny();
    }

    public String getPriorityName() {
        return priorityName;
    }
}
