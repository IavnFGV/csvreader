package interview.drozda.domain;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum JiraTeam {
    TEAM_BEAUJOLAIS("TEAM_BEAUJOLAIS"),
    TEAM_REGSERV("TEAM_REGSERV"),
    TEAM_LOIRE("TEAM_LOIRE"),
    TEAM_RHONE("TEAM_RHONE"),
    TEAM_TECH("TEAM_TECH"),
    TEAM_ALSACE("TEAM_ALSACE"),
    MISC("MISC");

    private String teamName;

    JiraTeam(String teamName) {
        this.teamName = teamName;
    }

    public static List<String> allTeamNames() {
        return Stream.of(JiraTeam.values())
                .map(JiraTeam::getTeamName)
                .collect(Collectors.toList());
    }

    public static Optional<JiraTeam> of(String toParse) {
        return Stream.of(JiraTeam.values())
                .filter(jiraPriority -> jiraPriority.teamName.equals(toParse))
                .findAny();
    }

    public String getTeamName() {
        return teamName;
    }
}
