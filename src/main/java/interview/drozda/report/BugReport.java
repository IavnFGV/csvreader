package interview.drozda.report;

import interview.drozda.csv.input.JiraInputRecord;
import interview.drozda.domain.JiraPriority;
import interview.drozda.report.line.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BugReport implements JiraReport {

    private static final String TOTAL_LINE_CAPTION = "Total";
    private static final String UNRESOLVED_LINE_CAPTION = "Unresolved";
    private List<BugReportLine> reportLines = new ArrayList<>();

    public BugReport() {
        initLines();
    }

    private void initLines() {
        reportLines.add(new HeaderBugReportLine());
        for (JiraPriority jiraPriority : JiraPriority.values()) {
            reportLines.add(new PriorityBugReportLine(jiraPriority));
        }
        reportLines.add(new TotalBugReportLine(TOTAL_LINE_CAPTION));
        reportLines.add(new UnresolvedBugReportLine(UNRESOLVED_LINE_CAPTION));
    }

    public void process(JiraInputRecord inputRecord) {
        reportLines.forEach(bugReportLine -> bugReportLine.processInput(inputRecord));
    }

    @Override
    public List<String[]> lines() {
        return reportLines.stream()
                .map(BugReportLine::toReportString)
                .collect(Collectors.toList());
    }
}
