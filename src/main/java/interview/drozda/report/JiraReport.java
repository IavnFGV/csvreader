package interview.drozda.report;

import java.util.List;

public interface JiraReport {

    List<String[]> lines();
}
