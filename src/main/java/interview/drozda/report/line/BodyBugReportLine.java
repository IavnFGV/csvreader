package interview.drozda.report.line;

import interview.drozda.csv.input.JiraInputRecord;
import interview.drozda.domain.JiraTeam;

import java.util.Arrays;
import java.util.stream.Stream;

public abstract class BodyBugReportLine extends BugReportLine {
    private int[] counters = new int[JiraTeam.values().length];

    BodyBugReportLine(String leftColumn) {
        super(leftColumn);
    }

    private int getIndex(JiraInputRecord inputRecord) {
        return inputRecord.getTeam().ordinal();
    }

    @Override
    public String[] toReportString() {
        return Stream.concat(Stream.of(leftColumn),
                Arrays.stream(counters).mapToObj(String::valueOf))
                .toArray(String[]::new);
    }

    void incrementCounter(JiraInputRecord inputRecord) {
        counters[getIndex(inputRecord)]++;
    }
}
