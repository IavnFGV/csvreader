package interview.drozda.report.line;

import interview.drozda.csv.input.JiraInputRecord;

public abstract class BugReportLine {
    final String leftColumn;

    BugReportLine(String leftColumn) {
        this.leftColumn = leftColumn;
    }

    public abstract void processInput(JiraInputRecord inputRecord);

    public abstract String[] toReportString();

}
