package interview.drozda.report.line;

import interview.drozda.csv.input.JiraInputRecord;
import interview.drozda.domain.Constants;
import interview.drozda.domain.JiraTeam;

import java.util.stream.Stream;

public class HeaderBugReportLine extends BugReportLine {

    public HeaderBugReportLine() {
        super(Constants.EMPTY_STRING);
    }

    @Override
    public String[] toReportString() {
        return Stream.concat(Stream.of(leftColumn), JiraTeam.allTeamNames().stream()).toArray(String[]::new);
    }

    @Override
    public void processInput(JiraInputRecord inputRecord) {
        // intentionally empty
    }
}
