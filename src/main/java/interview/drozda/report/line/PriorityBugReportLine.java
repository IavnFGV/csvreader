package interview.drozda.report.line;

import interview.drozda.csv.input.JiraInputRecord;
import interview.drozda.domain.JiraPriority;

import java.util.Objects;

public class PriorityBugReportLine extends BodyBugReportLine {

    private final JiraPriority jiraPriority;

    public PriorityBugReportLine(JiraPriority jiraPriority) {
        super(jiraPriority.getPriorityName());
        this.jiraPriority = jiraPriority;
    }

    @Override
    public void processInput(JiraInputRecord inputRecord) {
        if (Objects.equals(getPriorityForLine(), inputRecord.getPriority())) {
            incrementCounter(inputRecord);
        }
    }

    private JiraPriority getPriorityForLine() {
        return jiraPriority;
    }
}
