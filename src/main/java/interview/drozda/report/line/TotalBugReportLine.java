package interview.drozda.report.line;

import interview.drozda.csv.input.JiraInputRecord;

public class TotalBugReportLine extends BodyBugReportLine {
    public TotalBugReportLine(String leftColumn) {
        super(leftColumn);
    }

    @Override
    public void processInput(JiraInputRecord inputRecord) {
        incrementCounter(inputRecord);
    }

}
