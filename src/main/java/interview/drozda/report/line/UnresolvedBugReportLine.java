package interview.drozda.report.line;

import interview.drozda.csv.input.JiraInputRecord;

public class UnresolvedBugReportLine extends BodyBugReportLine {

    public UnresolvedBugReportLine(String leftColumn) {
        super(leftColumn);
    }

    @Override
    public void processInput(JiraInputRecord inputRecord) {
        if (!(inputRecord.isClosed())) {
            incrementCounter(inputRecord);
        }
    }

}
