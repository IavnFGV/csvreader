package interview.drozda;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import static java.nio.file.Files.lines;
import static java.nio.file.Paths.get;
import static java.util.stream.Collectors.joining;
import static org.junit.Assert.assertEquals;

public class AppTest {

    @Test
    public void shouldAnswerWithTrue() throws IOException, URISyntaxException {
        App.main(new String[]{"test_result.csv"});
        String resultFileAsString = lines(get("test_result.csv")).collect(joining());
        Path path = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource("bug-summary-expected.csv")).toURI());
        String expectedFileAsString = String.join("", Files.readAllLines(path));
        assertEquals(expectedFileAsString, resultFileAsString);
    }
}
