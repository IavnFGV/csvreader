package interview.drozda.csv.input;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class JiraBugsReaderTest {

    private static final int NUMER_OF_LINES_IN_TEST_FILE_WITHOUT_HEADER = 15;
    private List<JiraInputRecord> listToCheck = new ArrayList<>();

    private JiraBugsReader jiraBugsReader = new JiraBugsReader("/interview/drozda/test_input_for_BugsReader.csv", listToCheck::add);

    @Test
    public void shouldConsumeEveryLineFromFile() {

        jiraBugsReader.consumeInputFile();
        assertEquals(NUMER_OF_LINES_IN_TEST_FILE_WITHOUT_HEADER, listToCheck.size());
    }
}