package interview.drozda.csv.input.converter;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class JiraIsClosedStatusConverterTest {


    private static final String CLOSED_COMPLETE = "Closed - Complete";
    private static final String NEW_BUG = "New Bug";
    private JiraIsClosedStatusConverter jiraIsClosedStatusConverter = new JiraIsClosedStatusConverter();

    @Test
    public void shouldReturnTrueIfStatusIsClosed() {
        assertTrue((Boolean) jiraIsClosedStatusConverter.convert(CLOSED_COMPLETE));
    }

    @Test
    public void shouldReturnFalseIfStatusIsNotClosed() {
        assertFalse((Boolean) jiraIsClosedStatusConverter.convert(NEW_BUG));
    }

}