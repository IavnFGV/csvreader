package interview.drozda.csv.input.converter;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import org.junit.Test;

public class JiraTeamNameConverterExceptionTest {

    private static final String LINE_WITHOUT_TEAM_NAME = "SOME_TEXT_,SOME_TEXT";
    private JiraTeamNameConverter jiraTeamNameConverter = new JiraTeamNameConverter();

    @Test(expected = CsvDataTypeMismatchException.class)
    public void shouldThrowCsvConstraintViolationExceptionWhenStringDoesNotContainTeamname() throws CsvDataTypeMismatchException {
        jiraTeamNameConverter.convert(LINE_WITHOUT_TEAM_NAME);
    }
}