package interview.drozda.csv.input.converter;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import interview.drozda.domain.JiraTeam;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class JiraTeamNameConverterTest {

    @Parameterized.Parameter()
    public JiraTeam expectedTeamname;

    @Parameterized.Parameter(1)
    public String labelToParse;

    private JiraTeamNameConverter jiraTeamNameConverter = new JiraTeamNameConverter();

    @Parameterized.Parameters(name = "For string {1} we should get {0}")
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                        {JiraTeam.TEAM_BEAUJOLAIS, "TEAM_BEAUJOLAIS,"},
                        {JiraTeam.TEAM_BEAUJOLAIS, "SOME_TEXT,TEAM_BEAUJOLAIS,"},
                        {JiraTeam.TEAM_REGSERV, "SOME_TEXT,TEAM_BEAUJOLAIS,TEAM_REGSERV,"}, // last is preferred
                        {JiraTeam.TEAM_REGSERV, "SOME_TEXT,TEAM_REGSERV,"},
                        {JiraTeam.TEAM_LOIRE, "SOME_TEXT,TEAM_LOIRE,SOME_TEXT"},
                        {JiraTeam.TEAM_RHONE, "SOME_TEXT,TEAM_RHONE,"},
                        {JiraTeam.TEAM_TECH, "SOME_TEXT,TEAM_TECH,"},
                        {JiraTeam.TEAM_ALSACE, "SOME_TEXT,TEAM_ALSACE,"},
                        {JiraTeam.MISC, "SOME_TEXT, MISC,"}
                }
        );
    }

    @Test
    public void shouldReturnTeamName() throws CsvDataTypeMismatchException {
        Assert.assertEquals(expectedTeamname, jiraTeamNameConverter.convert(labelToParse));
    }
}