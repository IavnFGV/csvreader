package interview.drozda.report.line;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class HeaderBugReportLineTest {

    private HeaderBugReportLine headerBugReportLine = new HeaderBugReportLine();

    @Test
    public void shouldReturnEmptyLeftColumnAndListOfTeams() {
        Assert.assertEquals("[, TEAM_BEAUJOLAIS, TEAM_REGSERV, TEAM_LOIRE, TEAM_RHONE, TEAM_TECH, TEAM_ALSACE, MISC]",
                Arrays.asList(headerBugReportLine.toReportString()) + "");
    }
}