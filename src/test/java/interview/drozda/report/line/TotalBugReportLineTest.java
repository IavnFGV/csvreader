package interview.drozda.report.line;

import interview.drozda.csv.input.JiraInputRecord;
import interview.drozda.domain.JiraPriority;
import interview.drozda.domain.JiraTeam;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;

@RunWith(Parameterized.class)
public class TotalBugReportLineTest {
    private final String LINE_CAPTION = "LINE_CAPTION";
    @Parameterized.Parameter()
    public JiraTeam testTeam;
    private TotalBugReportLine totalBugReportLine = new TotalBugReportLine(LINE_CAPTION);
    private JiraInputRecord jiraInputRecord = new JiraInputRecord();
    private JiraPriority testPriority = JiraPriority.CRITICAL;
    private List<String> expectedLine;

    @Parameterized.Parameters(name = "Team {0}")
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                        {JiraTeam.TEAM_BEAUJOLAIS},
                        {JiraTeam.TEAM_REGSERV},
                        {JiraTeam.TEAM_LOIRE},
                        {JiraTeam.TEAM_RHONE},
                        {JiraTeam.TEAM_TECH},
                        {JiraTeam.TEAM_ALSACE},
                        {JiraTeam.MISC}
                }
        );
    }

    @Before
    public void prepareTestInput() {
        jiraInputRecord.setPriority(testPriority);
        jiraInputRecord.setTeam(testTeam);
        initExpectedString();
    }

    private void initExpectedString() {
        expectedLine = new ArrayList<>();
        expectedLine.add(LINE_CAPTION);
        for (JiraTeam jiraTeam : JiraTeam.values()) {
            if (Objects.equals(jiraTeam, testTeam)) {
                expectedLine.add(2 + "");
            } else {
                expectedLine.add(0 + "");
            }
        }
    }

    @Test
    public void shouldIncrementProperColumn() {
        totalBugReportLine.processInput(jiraInputRecord);
        totalBugReportLine.processInput(jiraInputRecord);

        Assert.assertEquals(String.join(",", expectedLine), String.join(",", totalBugReportLine.toReportString()));
    }
}