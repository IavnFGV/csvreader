package interview.drozda.report.line;

import interview.drozda.csv.input.JiraInputRecord;
import interview.drozda.domain.JiraTeam;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;

@RunWith(Parameterized.class)
public class UnresolvedBugReportLineTest {


    private final String LINE_CAPTION = "LINE_CAPTION";
    @Parameterized.Parameter()
    public JiraTeam testTeam;
    @Parameterized.Parameter(1)
    public boolean isClosed;
    private JiraInputRecord jiraInputRecord = new JiraInputRecord();
    private UnresolvedBugReportLine unresolvedBugReportLine = new UnresolvedBugReportLine(LINE_CAPTION);
    private List<String> expectedLine;

    @Parameterized.Parameters(name = "Team {0}")
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                        {JiraTeam.TEAM_BEAUJOLAIS, true},
                        {JiraTeam.TEAM_BEAUJOLAIS, false},
                        {JiraTeam.TEAM_REGSERV, true},
                        {JiraTeam.TEAM_REGSERV, false},
                        {JiraTeam.TEAM_LOIRE, true},
                        {JiraTeam.TEAM_LOIRE, false},
                        {JiraTeam.TEAM_RHONE, true},
                        {JiraTeam.TEAM_RHONE, false},
                        {JiraTeam.TEAM_TECH, true},
                        {JiraTeam.TEAM_TECH, false},
                        {JiraTeam.TEAM_ALSACE, true},
                        {JiraTeam.TEAM_ALSACE, false},
                        {JiraTeam.MISC, true},
                        {JiraTeam.MISC, false}
                }
        );
    }

    @Before
    public void prepareTestInput() {
        jiraInputRecord.setTeam(testTeam);
        jiraInputRecord.setClosed(isClosed);
        initExpectedString();
    }

    private void initExpectedString() {
        expectedLine = new ArrayList<>();
        expectedLine.add(LINE_CAPTION);
        for (JiraTeam jiraTeam : JiraTeam.values()) {
            if (Objects.equals(jiraTeam, testTeam) && !isClosed) {
                expectedLine.add(1 + "");
            } else {
                expectedLine.add(0 + "");
            }
        }
    }

    @Test
    public void shouldIncrementProperColumn() {
        unresolvedBugReportLine.processInput(jiraInputRecord);
        Assert.assertEquals(String.join(",", expectedLine), String.join(",", unresolvedBugReportLine.toReportString()));
    }
}